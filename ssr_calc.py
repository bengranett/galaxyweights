import sys
import os
import numpy as N
from vipers import catalogue,ssr_by_quadrant,ssr,selection,git
import logging
import argparse
import time
from collections import OrderedDict

import config

command_line = " ".join(sys.argv)

def main(outdir='.', sample='galaxyagn', knn=100, tag="", transform='rank', params=None, dbfile=config.DBFILE):
    """ """
    logging.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    logging.info("~~~~~~~~~~~ SSR Calculator ~~~~~~~~~~~~~~~~")
    logging.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    assert(selection.validate(sample))

    tag_nparam = "n%i"%len(params)

    header = OrderedDict()
    header["sample"] = sample
    header["knn"] = knn
    header["transform"] = transform
    header["params"] = ", ".join(params)
    header["nparams"] = len(params)
    header["tag"] = tag

    do_quad_ssr = False

    param_list = []
    constraints_list = []
    for pname in params:
        if pname == 'Q':
            do_quad_ssr = True
            continue
        param_list.append(pname)

        constraints_list.append(catalogue.constraint_string(pname))

    param_str = ",".join(param_list) 
    constraints_str = " and\n".join(constraints_list)

    with catalogue.Catalogue(dbfile=dbfile, config=config) as C:
        C.load('spec',['num','pointing','quadrant','zspec','zflg','selmag'])
        C.load('phot',['num','vmmpsflag','newflag','u_T07','g_T07','r_T07','i_T07','iy_T07','z_T07'])
        C.load('photsed',['num','zphot','Uj_abs','Bj_abs','Vj_abs'])

        # replace i with iy when i is missing
        C.process_iymag('phot')

        cmd = """select S.num, pointing, quadrant, zspec, zflg, %s
        from spec as S 
        join phot as P 
        join photsed as D 
        where S.num=P.num and S.num=D.num and 
        P.vmmpsflag='S' and P.newflag=1 and %s;"""%(param_str, constraints_str)

        logging.debug(cmd)

        results = C.query(cmd)
        print "Results:",len(results)
    if len(results)==0:
        logging.critical("DB query returned nothing!")
        exit()

    if do_quad_ssr:
        # Load quadrant average completeness
        quad_ssr = ssr_by_quadrant.get_ssr_by_quadrant(sample, dbfile=dbfile, config=config)

    objid = []
    phot = []
    targ_flag = []
    succ_flag = []
    for row in results:
        num,point,quad,zspec,zflg = row[:5]
        other_params = row[5:]

        # skip stars
        if selection.star(zflg, zspec):
            continue

        vec = []
        if do_quad_ssr:
            point = ssr_by_quadrant.unique_pointing(point)
            pointkey = ssr_by_quadrant.pointing_key(point,quad)
            q = quad_ssr[pointkey]
            vec = [q]

        for p in other_params:
            vec.append(p)

        objid.append(num)
        phot.append(vec)

        # targets means the denominator of the SSR fraction
        targ = 0
        if selection.select_targets(sample, zflg):
            targ = 1
        targ_flag.append(targ)

        # success is the numerator
        succ = 0
        if selection.select(sample, zflg):
            succ = 1
        succ_flag.append(succ)

    phot = N.array(phot)
    targ_flag = N.array(targ_flag).astype(bool)
    succ_flag = N.array(succ_flag).astype(bool)

    S = ssr.SSR(norm=transform)
    S.load(success_flag=succ_flag[targ_flag], phot_data=phot[targ_flag])
    S.train()

    # ensure success is a subset of targets
    assert(N.sum(succ_flag[targ_flag]) == N.sum(succ_flag))

    header['nsucc'] = N.sum(succ_flag)
    header['ntarg'] = N.sum(targ_flag)
    header['Tot SSR'] = N.sum(succ_flag)*1./N.sum(targ_flag)

    comp = S.compute(phot, knn=knn)
    print "mean compl",N.mean(comp[targ_flag])

    header['Mean SSR'] = N.mean(comp[targ_flag])
    header['Median SSR'] = N.median(comp[targ_flag])
    header['Min SSR'] = N.min(comp[targ_flag])
    header['Max SSR'] = N.max(comp[targ_flag])

    if tag != "":
        tag = "_"+tag
    outpath = "%s/ssr_%s_%s_%s%s.txt"%(outdir, sample, knn, tag_nparam, tag)
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    write_file(objid, comp, header=header, outpath=outpath, dbfile=dbfile)


def write_file(objid, ssr, header={}, outpath="ssr.txt", dbfile=config.DBFILE):
    """ """
    ssr_lookup = {}
    for i in range(len(objid)):
        ssr_lookup[objid[i]] = ssr[i]

    with catalogue.Catalogue(dbfile=dbfile, config=config) as C:
        num = C.query("select num from spec;")

    header['nobj'] = len(num)
    header['nobj with SSR'] = len(ssr)

    with file(outpath,'w') as out:
        print >>out, "#", command_line
        print >>out, "#", time.asctime()
        print >>out, "#", git.describe()
        for key,value in header.items():
            print >>out, "# %s: %s"%(key,value)
        print >>out, "# spec catalog: %s"%config.CATNAMES['spec']
        print >>out, "# phot catalog: %s"%config.CATNAMES['phot']
        print >>out, "# photo params: %s"%config.CATNAMES['photsed']
        print >>out, "# column 1: num"
        print >>out, "# column 2: SSR"

        for objnum in num:
            i = objnum[0]
            v = 0
            if i in ssr_lookup:
                v = ssr_lookup[i]
            print >>out, i, v


if __name__=="__main__":
    logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='VIPERS spectroscopic success rate calculator')
    parser.add_argument('--tag', metavar='t', default = "", help="Tag used to identify run")
    parser.add_argument('--outdir', default='out/', help="Output directory to store SSR catalogue.")
    parser.add_argument('--sample', default='galaxyagn', choices=selection.sample_list, help="""Sample for SSR calculation. Options include: "galaxy" flags 2-10, "galaxyagn" flags 2-10 & 12-20, "galaxylow" flags 1.5-10. (Default galaxyagn)""")
    parser.add_argument('--knn', default=100, type=int, help="Number of nearest neighbors to average over. (Default 100)")
    parser.add_argument('--transform', default='rank', choices=('rank','stddev'), help="Standardization transformation to apply for computing distances in the parameter space. (Default rank)")
    parser.add_argument('--params', metavar='p', default=('i_T07',), type=str, nargs='+')
    args = parser.parse_args()

    main(sample=args.sample, knn=args.knn, tag=args.tag, transform=args.transform, params=args.params, outdir=args.outdir)
