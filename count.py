import sys
import numpy as N
from vipers import catalogue
import config

with catalogue.Catalogue(dbfile='count.db', config=config) as C:
    C.load('spec',['num','zspec','zflg'])
    C.load('phot',['num','vmmpsflag','newflag'])

    cmd = """select count(*)
    from spec as S ;"""

    results = C.query(cmd)
    for r in results:
        print "everything", r


    cmd = """select count(*)
    from spec as S 
    join phot as P 
    where S.num=P.num and 
    P.vmmpsflag='S' and P.newflag=1 and zflg >= 0 and zflg < 20  ;"""

    results = C.query(cmd)
    for r in results:
        print "Targets", r



    cmd = """select count(*)
    from spec as S 
    join phot as P 
    where S.num=P.num and 
    P.vmmpsflag='S' and P.newflag=1 and zflg >= 2 and zflg < 20 and zspec > -1e-3 and zspec < 1e-3 ;"""

    results = C.query(cmd)
    for r in results:
        print "Stars", r
        star = r[0]


    cmd = """select count(*)
    from spec as S 
    join phot as P 
    where S.num=P.num and 
    P.vmmpsflag='S' and P.newflag=1 and zflg >= 2 and zflg < 10;"""

    results = C.query(cmd)
    for r in results:
        print "Galaxy", r

    cmd = """select count(*)
    from spec as S 
    join phot as P 
    where S.num=P.num and 
    P.vmmpsflag='S' and P.newflag=1 and ((zflg >= 2 and zflg < 10) or (zflg >=12 and zflg<20));"""

    results = C.query(cmd)
    for r in results:
        print "Galaxy+AGN", r

    cmd = """select count(*)
    from spec as S 
    join phot as P 
    where S.num=P.num and 
    P.vmmpsflag='S' and P.newflag=1 and zflg >= 1.5 and zflg < 10;"""

    results = C.query(cmd)
    for r in results:
        print "Galaxy low", r
