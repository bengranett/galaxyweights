import os
import pylab
import numpy as N
import argparse
import logging
from vipers import utils, selection, ssr_by_quadrant, catalogue
from vipers.catalogue import Catalogue
from vipers.ssr import SSR
import config

from scipy.ndimage import gaussian_filter1d
from matplotlib.ticker import MaxNLocator,MultipleLocator,AutoMinorLocator

def load_weights(path):
    lookup = {}
    for line in file(path):
        if line.startswith("#"): continue
        w = line.split()
        n = w[0]
        y = float(w[1])
        lookup[n] = y
    return lookup


def get_weights(lookup, num):
    """ """
    if lookup is None:
        return N.ones(len(num))

    out =N.zeros(len(num))
    for i in range(len(num)):
        w = lookup[num[i]]
        if w>0:
            w = 1./w
        out[i] = w
    return out

def get_adaptive_bins(x, minstep, batchsize=100):
    """ """
    xs = x[x.argsort()]
    i = 0
    ip = 0
    j = 0
    bins = [xs[0]]
    while i<len(x):
        if (xs[i]-bins[j]) > minstep:
            if i - ip>=batchsize:
                bins.append(xs[i])
                j+=1
                ip = i
        i+=1
    bins.append(xs[-1])

    bins = N.array(bins)

    h = bins[1:] - bins[:-1]

    nonzero = h>0
    norm = N.zeros(len(h))
    norm[nonzero] = 1./h[nonzero]

    return bins, norm


def plot(p1='photo_z', labels=None, ax=None, lookup=None, norm='rank', knn=100, vmin=0.5, vmax=1., sample='galaxyagn', 
    batchsize = 100, adaptive_bins=True, cmap='viridis_r'):
    """ """


    if labels is None:
        labels=p1
        # try:
        #     labels = "$%s_{%s}$"%tuple(p1.split("_"))
        # except:
        #     labels = "$%s$"%p1


    logging.info("Labels: %s",labels)

    do_quad_ssr = False

    if p1 == 'Q':
        do_quad_ssr = True

    p1name = ",%s"%p1
    if do_quad_ssr:
        # Load quadrant average completeness
        quad_ssr = ssr_by_quadrant.get_ssr_by_quadrant(sample)
        p1name = ""

    constraint = ""
    if not do_quad_ssr:
        constraint = "and "+catalogue.constraint_string(p1)

    with Catalogue(dbfile='vipers.db', config=config) as C:
        C.load('spec',['num','pointing','quadrant','zspec','zflg','selmag'])
        C.load('phot',['num','vmmpsflag','newflag','u_T07','g_T07','r_T07','i_T07','iy_T07','z_T07'])
        C.load('photsed',['num','zphot','Uj_abs','Bj_abs','Vj_abs','Ks_abs','NUV_abs','r_abs'])

        # replace i with iy when i is missing
        C.process_iymag('phot')

        cmd = """select S.num,pointing,quadrant,zspec, zflg%s
        from spec as S 
        join phot as P 
        join photsed as D 
        where S.num=P.num and S.num=D.num and 
        P.vmmpsflag='S' and P.newflag=1 and zflg >= 0 and zflg < 20         
        %s;"""%(p1name,constraint)

        print cmd

        data = C.query(cmd)
        logging.info("Results: %i",len(data))

    datasel = []
    for row in data:
        num,pointing,quad,zspec,zflg = row[:5]
        if selection.star(zflg, zspec):
            continue
        datasel.append(row)
    data = datasel

    if len(data)==0:
        return

    if do_quad_ssr:
        num,pointing,quad,zspec,zflg = utils.unzip(data)
    else:
        num,pointing,quad,zspec,zflg,param1 = utils.unzip(data)


    if do_quad_ssr:
        q = N.zeros(len(zspec),dtype='d')
        for i in range(len(q)):
            point = ssr_by_quadrant.unique_pointing(pointing[i])
            pointkey = ssr_by_quadrant.pointing_key(point,quad[i])
            q[i] = quad_ssr[pointkey]

        param1 = q

    sel = ((zflg>2)&(zflg<10)) | ((zflg>12)&(zflg<20))

    phot = param1

    xlow,xhigh = N.percentile(param1,[0.2,99.8])


    if adaptive_bins:
        minstep = (xhigh-xlow)/50.
        bins,norm = get_adaptive_bins(param1, minstep, batchsize)
    else:
        bins = N.linspace(xlow,xhigh,50)
        norm = N.ones(len(bins)-1)


    ssr_weight = get_weights(lookup, num[sel])

    print "len",len(num[sel]),len(ssr_weight)
    print "Mean weight",N.mean(ssr_weight)


    s,e = N.histogram(param1, bins, weights=param1)
    c,e = N.histogram(param1, bins)
    f,e = N.histogram(param1[sel], bins)
    fw,e = N.histogram(param1[sel], bins, weights=ssr_weight)

    # bin centers computed from the mean
    bc = s*1./c

    sr = f*1./c
    srw = fw*1./c

    h,e = N.histogram(param1, bins)
    h = h*1./h.max()
    # h = h*norm
    # hc = N.cumsum(h)
    # hc = hc * 1./hc[-1]


    ax.axhline(1, dashes=(3,2),c='grey')
    ax.plot(bc,sr,c='b',label="Completeness")
    ax.plot(bc,srw,c='firebrick',label="Weighted",zorder=10)
    ax.plot(bc,h,dashes=[4,1],c='grey',label="Distribution",zorder=0)

    ax.set_xlim(xlow,xhigh)
    # ax.text(.5,0.05,labels,transform=ax.transAxes,verticalalignment='bottom',horizontalalignment='center',fontsize=12,color='k')

    ax.set_xlabel(labels)
    # ax.ylabel("Success rate")

    # pylab.subplots_adjust(left=0.15,bottom=0.15,top=0.95)


def main(ssrfile="ssr_galaxyagn_100_n5_ugriz.txt", 
    params=("i_T07","zphot","Bj_abs","Uj_abs-Vj_abs","NUV_abs-r_abs","r_abs-Ks_abs","Q"), 
    title=None, show=True, savefile="fig.pdf"):

    # path = "plots/SR1D_%s_%s.pdf"%(p1,norm)
    # path = path.replace(" ","")
    # if not show and os.path.exists(path):
    #     logging.info('File already exists. Won\'t overwrite. %s', path)
    #     return


    pylab.figure(figsize=(13,2.5))

    lookup = load_weights(ssrfile)

    nparam = len(params)

    axprev = None
    subi = 1
    for i in range(nparam):
        ax = pylab.subplot(1,nparam,subi,sharey=axprev)
        axprev = ax
        plot(params[i],ax=ax,lookup=lookup)

        ax.set_ylim(0,1.2)
        ax.xaxis.set_major_locator(MaxNLocator(3))
        ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax.yaxis.set_major_locator(MultipleLocator(0.2))
        ax.yaxis.set_minor_locator(MultipleLocator(0.1))

        if subi==1:
            ax.set_ylabel("Success rate")
            ax.legend(loc='center left',frameon=False,fontsize=9)
        if subi > 1:
            pylab.setp(ax.get_yticklabels(),visible=False)

        subi+=1

    pylab.subplots_adjust(wspace=0,hspace=0,bottom=0.18,left=0.05,right=0.97)

    if title is not None:
        pylab.suptitle(title,horizontalalignment='left',x=0.05,fontsize=12)

    if show:
        pylab.show()
    else:
        pylab.savefig(savefile)
        pylab.close()


if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)
    # main(title="default")
    main('ssr_galaxyagn_100_n5_ugriz.txt', title="Trends: u-g, g-r, r-i, i-z, i")



