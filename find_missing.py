import sys
import numpy as N
from vipers import catalogue
import config

with catalogue.Catalogue(dbfile='vipers.db', config=config) as C:
    C.load('spec',['num','zspec','zflg'])
    C.load('phot',['num','vmmpsflag','newflag'])
    C.load('photsed',['num','zphot','Uj_abs','Bj_abs','Vj_abs'])

    # replace i with iy when i is missing
    C.process_iymag('phot')

    cmd = """select S.num, S.zflg, S.zspec, Uj_abs, P.i_T07, S.selmag, P.u_T07, P.g_T07, P.r_T07,P.z_T07
    from spec as S 
    join phot as P 
    join photsed as D 
    where S.num=P.num and S.num=D.num and 
    P.vmmpsflag='S' and P.newflag=1 and zflg >= 0 and zflg < 20 and 
    P.i_T07 > 0 and P.i_T07 < 30 and
    D.Bj_abs < -30
    ;"""

    res = C.execute(cmd)
    print "results",len(res)
    for r in res:
        print r


