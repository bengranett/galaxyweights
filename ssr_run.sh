python2.7 ssr_calc.py --param i_T07 --tag iT07
python2.7 ssr_calc.py --param i_T07 Bj_abs --tag iT07B
python2.7 ssr_calc.py --param i_T07 Bj_abs Uj_abs-Vj_abs --tag iT07BUV
python2.7 ssr_calc.py --param i_T07 Bj_abs Uj_abs-Vj_abs Q --tag iT07BUVQ

python2.7 ssr_calc.py --param u_T07-g_T07 g_T07-r_T07 r_T07-i_T07 i_T07-z_T07 i_T07 --tag ugriz
python2.7 ssr_calc.py --param u_T07-g_T07 g_T07-r_T07 r_T07-i_T07 i_T07-z_T07 i_T07 Q --tag ugrizq

python2.7 ssr_calc.py --param selmag --tag selmag --sample=galaxyagn
python2.7 ssr_calc.py --param selmag --tag selmag --sample=galaxy
python2.7 ssr_calc.py --param selmag --tag selmag --sample=galaxylow
