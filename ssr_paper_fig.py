import os
import pylab
import numpy as N
import argparse
import logging
from vipers import utils,ssr_by_quadrant,selection
from vipers.catalogue import Catalogue
from vipers.ssr import SSR
import config
from scipy.ndimage import gaussian_filter,gaussian_filter1d
from matplotlib.ticker import MaxNLocator,MultipleLocator,AutoMinorLocator
from matplotlib.colors import BoundaryNorm
import matplotlib

from sklearn.neighbors import KDTree
from sklearn.neighbors.kde import KernelDensity
from scipy.ndimage.interpolation import zoom
import voronoi


def discrete_cmap(n, base_cmap=None):
    """Create an N-bin discrete colormap from the specified input map"""

    # Note that if base_cmap is a string or None, you can simply do
    #    return plt.cm.get_cmap(base_cmap, N)
    # The following works for string, None, or a colormap instance:

    base = matplotlib.cm.get_cmap(base_cmap)
    color_list = base(N.linspace(0, 1, n))
    cmap_name = base.name + str(N)
    return ListedColormap(cmap_name, color_list, n)

def nn_density(data, bins, knn=50, threshold=3):
    """ """
    xc = (bins[0][1:]+bins[0][:-1])/2.
    yc = (bins[1][1:]+bins[1][:-1])/2.

    xx,yy = N.meshgrid(yc,xc)
    shape = xx.shape
    xx = xx.flatten()
    yy = yy.flatten()
    points = N.transpose([yy,xx])

    lookuptree = KDTree(data)
    d, ind = lookuptree.query(points, k=knn)

    mu = N.mean(d,axis=1)
    sig = N.std(d,axis=1)
    a = N.abs(mu/sig)

    # pylab.figure()
    # pylab.imshow(a.reshape(shape))
    # pylab.colorbar()
    # pylab.show()

    bad = a > threshold

    d = N.max(d,axis=1)
    dens = 1./(d*d)

    dens[bad]=0

    norm = len(data)*1./N.sum(dens)
    dens *= norm


    dens = dens.reshape(shape).T

    # pylab.figure()
    # pylab.imshow(N.log(dens))
    # pylab.show()

    return dens

def kernel_density(data, bins):
    """ """
    xc = (bins[0][1:]+bins[0][:-1])/2.
    yc = (bins[1][1:]+bins[1][:-1])/2.

    xx,yy = N.meshgrid(yc,xc)
    shape = xx.shape
    xx = xx.flatten()
    yy = yy.flatten()
    points = N.transpose([yy,xx])

    min = points.min(axis=0)
    max = points.max(axis=0)
    width = max - min

    print "min",min,max,width
    nsamp = N.array([len(xc),len(yc)])

    points = (points - min)*1./width

    print "KDE"
    KD = KernelDensity(bandwidth=5./nsamp[0], kernel='linear')
    KD.fit((data-min)*1./width)
    print "evaluating density"
    dens = KD.score_samples(points)
    print "done"

    norm = len(data)*1./N.sum(dens)
    print "norm",norm
    # dens *= norm

    dens = dens.reshape(shape).T

    dens = N.exp(dens)

    # pylab.figure()
    # pylab.imshow(dens)
    # pylab.colorbar()
    # # pylab.imshow(N.log(dens))
    # pylab.show()

    return dens


def main(ax, p1='photo_z', p2='i_T07', labels=None, limits=None, norm='rank', knn=100, vmin=0.4, vmax=1., sample='galaxyagn', cmap='viridis_r'):
    """ """

    pnames = p1,p2
    if labels is None:
        labels = []
        for p in pnames:
            labels.append("$%s_{%s}$"%tuple(p.split("_")))


    logging.info("Labels: %s",labels)

    do_quad_ssr = False

    if p1 == 'Q':
        do_quad_ssr = True
        pnames = p2,p2
        qi = 0
    elif p2 == 'Q':
        do_quad_ssr = True
        pnames = p1,p1
        qi = 1

    if do_quad_ssr:
        # Load quadrant average completeness
        quad_ssr = ssr_by_quadrant.get_ssr_by_quadrant(sample)


    with Catalogue(dbfile='vipers.db', config=config) as C:
        C.load('spec',['num','pointing','quadrant','zspec','zflg'])
        C.load('phot',['num','vmmpsflag','newflag','i'])
        C.load('photsed',['num','zphot','Uj_abs','Bj_abs','Vj_abs'])

        cmd = """select pointing,quadrant,zspec, zflg, %s, %s
        from spec as S 
        join phot as P 
        join photsed as D 
        where S.num=P.num and S.num=D.num and 
        P.vmmpsflag='S' and P.newflag=1 and zflg >= 0 and zflg < 20 and 
        i>0 and i<100 and 
        Bj_abs<0 and Bj_abs>-30 and
        Vj_abs<0 and Vj_abs>-30 and
        Uj_abs<0 and Uj_abs>-30;"""%pnames

        data = C.query(cmd)
        logging.info("Results: %i",len(data))

    pointing,quad,zspec,zflg,param1,param2 = utils.unzip(data)

    if do_quad_ssr:
        q = N.zeros(len(zspec),dtype='d')
        for i in range(len(q)):
            point = ssr_by_quadrant.unique_pointing(pointing[i])
            pointkey = ssr_by_quadrant.pointing_key(point,quad[i])
            q[i] = quad_ssr[pointkey]

        if qi == 0:
            param1 = q
        else:
            param2 = q


    succ = selection.apply(sample, zflg)
    print "mean",N.sum(succ)*1./len(succ)

    phot = N.transpose([param1, param2])

    S = SSR(norm=norm)
    S.load(success_flag=succ, phot_data=phot)
    S.train()

    comp = S.compute(phot, knn=knn)
    print "mean compl",N.mean(comp)

    xlow,xhigh = limits[0]
    ylow,yhigh = limits[1]

    print "range", p1, N.percentile(param1,[0.2,99.8])
    print "range", p2, N.percentile(param2,[0.2,99.8])

    x = N.linspace(xlow,xhigh,200)
    y = N.linspace(ylow,yhigh,200)
    xlr = N.linspace(xlow,xhigh,50)
    ylr = N.linspace(ylow,yhigh,50)

    xc = (x[1:]+x[:-1])/2.
    yc = (y[1:]+y[:-1])/2.

    xx,yy = N.meshgrid(xc,yc)
    shape = xx.shape
    xx = xx.flatten()
    yy = yy.flatten()
    grid = N.transpose([xx,yy])

    f = S.compute(grid,knn=knn)

    f = f.reshape(shape)

    data = N.vstack([param1,param2]).T
    print data.shape
    print param1.shape,param2.shape
    print grid.shape
    # hsmoo = voronoi.density(data, (x,y), cvt_loops=5)
    hsmoo = nn_density(data, (x,y), knn=400, threshold=4)
    # hsmoo = kernel_density(data, (x,y))
    hsmoo = gaussian_filter(hsmoo,2)


    # h,ex,ey = N.histogram2d(param2,param1,(ylr,xlr))
    # h = gaussian_filter(h,2)

    # plev = N.array([0.99, 0.90,0.5,0.1])
    plevmod = N.array([0.99, 0.90,0.5,0.1])*0.99

    levels = utils.lowerwater(hsmoo, plevmod)
    # levels2 = utils.lowerwater(h, plev)

    ii = hsmoo < levels[0]
    f[ii] = float('nan')

    ok = N.isfinite(f)
    print "range:",f[ok].min(),f[ok].max()

    ext = (x.min(),x.max(),y.min(),y.max())

    bounds = N.linspace(0.4,1,13)
    norm = BoundaryNorm(bounds, cmap.N)

    # im = ax.imshow(h, extent=ext, aspect='auto', origin='lower', interpolation='nearest', cmap=cmap)
    im = ax.imshow(f, extent=ext, norm=norm, vmin=vmin, vmax=vmax, aspect='auto', origin='lower', interpolation='nearest', cmap=cmap)
    # ax.contour(h, levels=levels2, colors='grey', extent=ext, origin='lower')
    ax.contour(hsmoo, levels=levels, colors='k', extent=ext, origin='lower')

    ax.set_xlabel(labels[0])
    # ax.set_ylabel(labels[1])
    return im



if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)

    cmap = pylab.get_cmap('plasma')

    fw = 7
    fh = 2.5
    aspect = fw*1./fh
    fig = pylab.figure(figsize=(fw,fh))

    left = 0.061
    right = 0.06

    cbw = .015
    bot = 0.136

    w = (1-left-right-cbw)/3.
    h = w * aspect

    print w,h/aspect

    top = min(0,1-h-bot)
    assert(h+bot <= 1)


    ax1 = pylab.axes((left,bot,w,h))
    ax2 = pylab.axes((left+w,bot,w,h))
    ax3 = pylab.axes((left+2*w,bot,w,h))
    ax4 = pylab.axes((left+3*w,bot,cbw,h))

    pylab.savefig("fig.pdf")

    limits_pz=0.3,1.3
    limits_uv = 0.25,2.3
    limits_b = -23.1,-18.5
    limits_i = 19.5,22.6

    main(ax1,'zphot','Uj_abs - Vj_abs',labels=('Photo redshift','$Restframe\ U - V$'),limits=(limits_pz,limits_uv),cmap=cmap)
    im = main(ax2,'i','Uj_abs - Vj_abs',labels=('Apparent $i$ mag','$U - V$'),limits=(limits_i,limits_uv),cmap=cmap)
    im = main(ax3,'Bj_abs','Uj_abs - Vj_abs',labels=('Restframe $B$','$U - V$'),limits=(limits_b,limits_uv), cmap=cmap)

    cbar = fig.colorbar(im, cax=ax4)

    ax1.set_ylabel("Restframe $U-V$")
    ax4.set_ylabel("Success rate")

    pylab.setp(ax2.get_yticklabels(),visible=False)
    pylab.setp(ax3.get_yticklabels(),visible=False)

    ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
    ax2.yaxis.set_minor_locator(MultipleLocator(0.1))
    ax3.yaxis.set_minor_locator(MultipleLocator(0.1))

    ax1.yaxis.set_major_locator(MultipleLocator(0.5))
    ax2.yaxis.set_major_locator(MultipleLocator(0.5))
    ax3.yaxis.set_major_locator(MultipleLocator(0.5))

    ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
    ax2.xaxis.set_minor_locator(MultipleLocator(0.5))
    ax3.xaxis.set_minor_locator(MultipleLocator(0.5))

    ax1.xaxis.set_major_locator(MultipleLocator(0.2))
    ax2.xaxis.set_major_locator(MultipleLocator(1.0))
    ax3.xaxis.set_major_locator(MultipleLocator(1))

    # ax4.yaxis.set_major_locator(MultipleLocator(0.1))
    ax4.yaxis.set_ticks(N.linspace(0,1,7))
    a,b = cbar.get_clim()
    ax4.yaxis.set_ticklabels(N.linspace(a,b,7))

    # main('M_Uj - M_V','M_B',labels=('$U - V$','$B$'), knn=100,cmap=cmap)
    # main('M_Uj - M_V','i_T07',labels=('$U - V$','$i$'), knn=100,cmap=cmap)

    # main('photo_z','i_T07',labels=('$photoz$','$i$'),cmap=cmap)

    # main("Q","i_T07",labels=('Mean quadrant completeness','$i$'),cmap=cmap)
    pylab.savefig("SSRfig_i.pdf")
    pylab.savefig("SSRfig_i.eps")
