import pylab
import numpy as N
import sys


markers='o','x'
for i,file in enumerate(sys.argv[1:]):
    x,y = N.loadtxt(file, unpack=True)[-2:]
    print file,len(x)

    pylab.plot(x,y,markers[i],mfc='none',c='k')

pylab.show()