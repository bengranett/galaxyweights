import os
import pylab
import numpy as N
import argparse
import logging
from vipers import utils, ssr_by_quadrant, selection, catalogue
from vipers.catalogue import Catalogue
from vipers.ssr import SSR
from scipy.ndimage import gaussian_filter1d
from matplotlib.ticker import MaxNLocator,MultipleLocator,AutoMinorLocator
from matplotlib.colors import BoundaryNorm

import config

def main(p1='photo_z', p2='i_T07', labels=None, show=True, norm='rank', knn=100, vmin=0.4, vmax=1., sample='galaxyagn', cmap='plasma'):
    """ """
    cmap = pylab.get_cmap(cmap)


    path = "plots/SR_%s_%s_%s.pdf"%(p1,p2,norm)
    path = path.replace(" ","")
    if show is False and os.path.exists(path):
        logging.info('File already exists. Won\'t overwrite. %s', path)
        return

    pnames = p1,p2
    if labels is None:
        # labels = []
        # for p in pnames:
        #     try:
        #         labels.append("$%s_{%s}$"%tuple(p.split("_")))
        #     except:
        #         labels.append(p)
        labels = pnames


    logging.info("Labels: %s",labels)

    do_quad_ssr = False

    if p1 == 'Q':
        do_quad_ssr = True
        pnames = p2,p2
        qi = 0
    elif p2 == 'Q':
        do_quad_ssr = True
        pnames = p1,p1
        qi = 1

    if do_quad_ssr:
        # Load quadrant average completeness
        quad_ssr = ssr_by_quadrant.get_ssr_by_quadrant(sample)


    constraint = ""
    if p1 != 'Q':
        constraint = catalogue.constraint_string(p1)
    if p2 != 'Q':
        constraint += " and "+catalogue.constraint_string(p2)


    with Catalogue(dbfile='vipers.db', config=config) as C:
        C.load('spec',['num','pointing','quadrant','zspec','zflg','selmag'])
        C.load('phot',['num','vmmpsflag','newflag','i_T07','iy_T07'])
        C.load('photsed',['num','zphot','Uj_abs','Bj_abs','Vj_abs','Ks_ABS','NUV_ABS','r_ABS'])

        # replace i with iy when i is missing
        C.process_iymag('phot')

        cmd = """select pointing,quadrant,zspec, zflg, %s, %s
        from spec as S 
        join phot as P 
        join photsed as D 
        where S.num=P.num and S.num=D.num and 
        P.vmmpsflag='S' and P.newflag=1 and zflg >= 0 and zflg < 20 and 
        %s
        ;"""%(pnames[0],pnames[1],constraint)

        data = C.query(cmd)
        logging.info("Results: %i",len(data))

    pointing,quad,zspec,zflg,param1,param2 = utils.unzip(data)

    if do_quad_ssr:
        q = N.zeros(len(zspec),dtype='d')
        for i in range(len(q)):
            point = ssr_by_quadrant.unique_pointing(pointing[i])
            pointkey = ssr_by_quadrant.pointing_key(point,quad[i])
            q[i] = quad_ssr[pointkey]

        if qi == 0:
            param1 = q
        else:
            param2 = q


    succ = selection.apply(sample, zflg)

    phot = N.transpose([param1, param2])

    S = SSR(norm=norm)
    S.load(success_flag=succ, phot_data=phot)
    S.train()

    comp = S.compute(phot, knn=knn)
    print "mean compl",N.mean(comp)

    xlow,xhigh = N.percentile(param1,[0.5,99.8])
    ylow,yhigh = N.percentile(param2,[0.5,99.8])

    x = N.linspace(xlow,xhigh,50)
    y = N.linspace(ylow,yhigh,50)
    xc = (x[1:]+x[:-1])/2.
    yc = (y[1:]+y[:-1])/2.

    xx,yy = N.meshgrid(xc,yc)
    shape = xx.shape
    xx = xx.flatten()
    yy = yy.flatten()
    grid = N.transpose([xx,yy])

    f = S.compute(grid,knn=knn)

    f = f.reshape(shape)

    h,ex,ey = N.histogram2d(param2,param1,(y,x))
    hsmoo = gaussian_filter1d(h,2)

    plev = N.array([0.99, 0.90,0.5,0.1])
    levels = utils.lowerwater(hsmoo, plev)


    ii = hsmoo < 2
    f[ii] = float('nan')

    ok = N.isfinite(f)
    print "range:",f[ok].min(),f[ok].max()

    ext = (x.min(),x.max(),y.min(),y.max())

    pylab.figure(figsize=(4,3))

    bounds = N.linspace(0.4,1,13)
    norm = BoundaryNorm(bounds, cmap.N)

    pylab.imshow(f, extent=ext, norm=norm, vmin=vmin, vmax=vmax, aspect='auto', origin='lower', interpolation='nearest', cmap=cmap)
    pylab.colorbar(label="Success rate")
    pylab.contour(hsmoo, levels=levels, colors='k', extent=ext, origin='lower')

    pylab.xlabel(labels[0])
    pylab.ylabel(labels[1])

    pylab.subplots_adjust(left=0.18,bottom=0.15,top=0.95)

    if show is False:
        pylab.savefig(path)
        pylab.close()


if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)

    cmap = pylab.get_cmap('rainbow',10)
    

    # main('M_Uj - M_V','M_B',labels=('$U - V$','$B$'), knn=100,cmap=cmap)
    # main('M_Uj - M_V','i_T07',labels=('$U - V$','$i$'), knn=100,cmap=cmap)

    # main('photo_z','M_Uj - M_V',labels=('$photoz$','$U - V$'),cmap=cmap)
    # main('photo_z','i_T07',labels=('$photoz$','$i$'),cmap=cmap)

    main('r_abs-Ks_abs','NUV_abs-r_abs',labels=('r-Ks','NUV-r'))
    pylab.show()

