# VIPERS Statistical weights #


### What is this repository for? ###

* This is a package for computing the statistical weights for VIPERS.
* The target sampling rate (TSR) and spectroscopic success rate (SSR) are included.
* For details see the VIPERS publications (http://vipers.inaf.it)

### How do I get set up? ###

* You will need python2.7 with the following modules: numpy, scipy, pyfits
* The computation of TSR needs a custom version of DTFE which is included, but you will need to compile it.
* Configuration is specified in ``config.py`` and command line arguments.
* We accept VIPERS catalogue files in ``fits`` format as output by the VIPERS database.  Just output all columns and we will do the rest. 
* At the moment the only catalogue format accepted is ``fits``.  Still need to add the capability to input other formats for use with mocks.
* Also pick up the [VIPERS mask files](https://bitbucket.org/bengranett/galaxyweights/downloads/vipers_masks.tar.gz).  You will need at least the field mask to compute TSR.


### Example runs ###

* The default TSR calculation may be run like so:
```
#!sh

python2.7 tsr_calc.py
```

* Here is an SSR example that uses 4 photometric parameters:
```
#!sh

python2.7 ssr_calc.py --param i_T07 Bj_abs Uj_abs-Vj_abs Q --tag iT07BUVQ
```

### Contacts ###
The statistical weights algorithms were developed by the VIPERS team.  This code repository is maintained by Ben Granett.  Contributions are welcome.