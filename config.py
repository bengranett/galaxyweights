import os
import vipers

# paths will be relative to current directory.
HOME = os.path.dirname(os.path.realpath(__file__)) + "/"

TMPDIR = HOME+'tmp/'

FIELDS = ['W1', 'W4']

CATNAMES = {'spec': HOME+'data/%s_SPECTRO_V7_1.fits',
            'phot': HOME+'data/%s_PHOT.fits',
            'photsed': HOME+'data/%s_VIPERS_T0007_ZPHOT_PYSPARAM.fits'
            }
DBFILE = TMPDIR + 'vipers.db'

##########################################
############ TSR configuration ###########

FIELDMASK = HOME+'data/vipersmask_v6_10042015/vipers_%s_v6.0.reg'
PHOTMASK = HOME+'data/vipers_photo_samhain/photo_%s.reg'

PHOT_OUT = TMPDIR+'phot_%s.txt'
SPEC_OUT = TMPDIR+'spec_%s.txt'

VENICECMD = '{ext}/venice/venice'.format(ext=vipers.EXT)
DTFECMD = '{ext}/DTFE_1.1.1_2D/DTFE'.format(ext=vipers.EXT)

##########################################
############ SSR configuration ###########
