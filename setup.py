try:
    from setuptools import setup, Extension
except ImportError:
    from distutils.core import setup, Extension

setup(
    name = 'galaxyweights',
    version = '0.1.dev0',
    description = '',
    author = 'Ben Granett',
    author_email = 'ben.granett@brera.inaf.it',
    url = 'none',
    download_url = 'none',
    packages = ['vipers',],
    license = 'Creative Commons Attribution-Noncommercial-Share Alike license',
    install_requires = [
        'numpy',
	],
    scripts = ['tsr_calc.py', 'ssr_calc.py'],
)
