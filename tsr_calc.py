import sys
import os
import numpy as N
import pyfits
import logging
import argparse
import time

from vipers import tsr, selection, git
import config

command_line = " ".join(sys.argv)

def run(TSR, args, config):
    """ """
    lookup = {}
    lookup_meta = {}
    nphot = 0
    nspec = 0
    for field in args.fields:
        logging.info("           Starting field %s"%field)
        field_lookup, info = TSR.compute(field)
        logging.debug(info)
        nphot += info['nphot']
        nspec += info['nspec']
        for key,value in field_lookup.items():
            lookup[key] = value[0]
            lookup_meta[key] = value[1],value[2]

    data = []
    for field in args.fields:

        if args.target is not None:
            targ_num = TSR._load_ascii(path=args.target, applymask=False)[0]
        else:
            fits = pyfits.open(config.CATNAMES['spec']%field)
            targ_num = fits[1].data['num']

        for num in targ_num:
            if lookup.has_key(num):
                t = lookup[num]
                m = lookup_meta[num]
            else:
                t = 0
                m = 0,0
            data.append((num,t,m[0],m[1]))

    tsrvalues = N.array(lookup.values())
    tsrvalues = tsrvalues[tsrvalues>0]

    return data, tsrvalues, nphot, nspec




def main(args):
    """ """
    logging.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    logging.info("~~~~~~~~~~~ TSR Calculator ~~~~~~~~~~~~~~~~")
    logging.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    if args.parent is not None or args.target is not None:
        if args.parent is None:
            raise Exception("You need to specify the parent catalogue with --parent")
        if args.target is None:
            raise Exception("You need to specify the target catalogue with --target")

        if not os.path.exists(args.parent):
            raise Exception("Parent catalogue does not exist: %s"%args.parent)
        if not os.path.exists(args.target):
            raise Exception("Target catalogue does not exist: %s"%args.target)

        args.sample = selection.MOCK
    else:
        if args.sample == selection.MOCK:
            raise Exception("Mock sample was specified but no catalogues given.")

    TSR = tsr.TSR(sample=args.sample, applyphotomask=args.photomask, 
        aperture=args.aperture, usevenice=args.usevenice, 
        parent = args.parent, target = args.target,
        config=config)

    tag = args.tag
    if len(tag)>0:
        tag = "_"+tag

    if args.out is None:
        outfile = "%s/tsr_%s%s.txt"%(args.outdir, args.sample, tag)
        if not os.path.exists(args.outdir):
            os.mkdir(args.outdir)
    else:
        outfile = args.out

    data, tsrvalues, nphot, nspec = run(TSR, args, config)

    tot_tsr = nspec *1./ nphot

    with file(outfile, "w") as out:
        print >>out, "#", command_line
        print >>out, "#", time.asctime()
        print >>out, "#", git.describe()
        print >>out, "# tag: %s"%args.tag
        print >>out, "# sample: %s"%args.sample
        print >>out, "# apply photomask: %s"%args.photomask
        print >>out, "# aperture: %s %s"%args.aperture
        print >>out, "# usevenice: %s"%args.usevenice
        print >>out, "# spec catalog: %s"%config.CATNAMES['spec']
        print >>out, "# phot catalog: %s"%config.CATNAMES['phot']
        print >>out, "# nphot %i"%nphot
        print >>out, "# nspec %i"%nspec
        print >>out, "# total TSR: %f"%tot_tsr
        print >>out, "# mean TSR: %f"%N.mean(tsrvalues)
        print >>out, "# mean 1/TSR: %f"%N.mean(1./tsrvalues)
        print >>out, "# tsr count: %i"%len(tsrvalues)
        print >>out, "# object count: %i"%len(data)
        print >>out, "# column 1: num"
        print >>out, "# column 2: TSR"
        for num, t, a, b in data:
            print >>out, num, t

    logging.info("Wrote %s", outfile)

if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='VIPERS target sampling rate rate calculator')
    parser.add_argument('--tag', metavar='t', default = "", help="Tag used to identify run")
    parser.add_argument('--outdir', default='out/', help="Output directory to store SSR catalogue. (default:out)")
    parser.add_argument('--sample', default='galaxyagn', choices=selection.sample_list, help="""Sample for SSR calculation. Options include: "galaxy" flags 2-10, "galaxyagn" flags 2-10 & 12-20, "galaxylow" flags 1.5-10. (Default galaxyagn)""")
    parser.add_argument('--photomask', action='store_true', help="Apply photometric mask (default: no)")
    parser.add_argument('--aperture', default=(40.,100.), help="TSR width, height of rectangular aperture (arcsec) (default:40,100)")
    parser.add_argument('--usevenice', action='store_true', help="Process mask with VENICE otherwise use holey.")

    parser.add_argument('--fields', default=['W1','W4'], nargs='+', help="Fields to do (default: W1 W4)")
    parser.add_argument('--parent', default=None, type=str, help="Catalogue of parent sample.")
    parser.add_argument('--target', default=None, type=str, help="Catalogue of target sample.")
    parser.add_argument('--out', default=None, type=str, help="Output file")

    args = parser.parse_args()

    main(args)

