import os
import numpy as N
import logging
import pyfits
import sqlite3
from collections import OrderedDict

# HOME = os.path.dirname(os.path.realpath(__file__))
# DATA_ROOT = '%s/data'%HOME
# FIELDS = 'W1','W4'
# CATNAMES = {'spec': '%s_SPECTRO_V7_1.fits',
#             'phot': '%s_PHOT.fits',
#             'photsed': '%s_VIPERS_T0007_ZPHOT_PYSPARAM.fits'
#             }

limits = {'Uj_abs': (-30,0),
        'Bj_abs': (-30,0),
        'Vj_abs': (-30,0),
        'i': (0,30),
        'i_T07': (0,30),
        'selmag':(0,30),
        'u_T07': (0,30),
        'g_T07': (0,30),
        'r_T07': (0,30),
        'z_T07': (0,30),
        'zphot': (-0.1,10),
        'NUV_abs': (-30,0),
        'Ks_abs': (-30,0),
        'r_abs': (-30,0),
    }

KEY_TRANSLATION = {'id':'num',}

def constraint_string(pname):
    constraints_list = []
    for sub in pname.split("-"):
        sub = sub.strip()
        if not sub in limits:
            logging.critical("No limits known for parameter %s! You should add the limit in catalogue.py", sub)
            lim = 0,100
            raise Exception("No limits known for parameter %s! You should add the limit in catalogue.py"%sub)
        else:
            lim = limits[sub]
        constraints_list.append("%s between %f and %f"%(sub, lim[0], lim[1]))


    return " and\n".join(constraints_list)


def determine_type(t):
    """ Determine the SQL type for a numpy-style type string.

    Parameters
    ----------
    t : str
        the numpy style type string
    
    Returns
    -------
    SQL_TYPE : str
        SQL type string (TEXT, REAL, INTEGER)
    python_type : type object
        corresponding python type object
    """
    t = str(t).lower()
    if 's' in t:
        return 'TEXT', str
    elif 'f' in t:
        return 'REAL', float
    elif 'i' in t:
        return 'INTEGER', int
    else:
        raise Exception("Unable to parse type: %s"%t)



class Catalogue(object):
    """ """

    def __init__(self, dbfile=':memory:', data_root="data", config=None):
        """ """
        try:
            os.mkdir(os.path.dirname(dbfile))
        except:
            pass
        self.config = config
        self.dbfile = dbfile
        self.conn = None
        self.cursor = None


    def __enter__(self):
        """ """
        self.conn = sqlite3.connect(self.dbfile)
        self.cursor = self.conn.cursor()
        return self

    def __exit__(self, type, value, traceback):
        """ """
        self.conn.close()

    def execute(self, cmd):
        """ """
        self.cursor.execute(cmd)
        return self.cursor.fetchall()

    query = execute

    def insert(self, table_name, data):
        """ """
        nparam = len(data[0])
        place_holder = ','.join(['?']*nparam)
        insertcmd = 'INSERT INTO %s VALUES (%s)'%(table_name, place_holder)
        logging.debug(insertcmd)
        self.cursor.executemany(insertcmd, data)
        self.conn.commit()

    def get_table_names(self):
        tables = []
        for row in self.query("""SELECT name FROM sqlite_master WHERE type='table'"""):
            tables.append(row[0])
        return tables


    def table_exists(self, table_name):
        """ """
        tables = self.get_table_names()
        return table_name in tables


    def load(self, catname='spec', param_names=[], fields=['W1','W4']):
        """ """
        if self.cursor is None:
            self.__enter__()

        if self.table_exists(catname):
            logging.debug("Table already exists %s.  Skipping load..."%catname)
            return

        if len(param_names) == 0:
            return None

        table_created = False
        tablename = catname

        cat_list = []
        for field in fields:
            path = self.config.CATNAMES[catname]%field
            # path = '%s/%s'%(DATA_ROOT, cat)
            # path = catpath%field

            data = pyfits.open(path)[1].data

            cat = []

            for key in param_names:
                fits_key = key
                try:
                    data[fits_key]
                except KeyError:
                    for k,v in KEY_TRANSLATION.items():
                        if v.lower() == key.lower():
                            try:
                                data[k]
                            except KeyError:
                                continue
                            fits_key = k

                try:
                    x = data[fits_key]
                except KeyError:
                    print "column names:",data.names
                    raise Exception("%s: Column does not exist: %s"%(path,fits_key))

                type_info = determine_type(x[0].dtype)
                cat.append((key, x, type_info))

            if not table_created:
                table_def = []
                for key, data, type_info in cat:
                    sql_type = type_info[0]
                    constraint = ""
                    if key == "num":
                        constraint = "PRIMARY KEY"
                    table_def.append("%s %s %s"%(key, sql_type, constraint))
                table_def = ",".join(table_def)

                cmd = "create table if not exists %s (%s)"%(tablename, table_def)
                logging.debug(cmd)

                self.cursor.execute(cmd)
                table_created = True

            nrows = len(cat[0][1])
            data = []

            for i in range(nrows):
                stuff = []
                for key, value, type_info in cat:
                    py_type = type_info[1]
                    stuff.append(py_type(value[i]))
                data.append(tuple(stuff))

            self.insert(tablename, data)

    def process_iymag(self, table_name):
        """ """
        cmd = "update %s set i_T07 = iy_T07 where i_T07 < 0"%table_name;
        logging.debug(cmd)
        r = self.cursor.execute(cmd)
        logging.debug("update result: %s",r)


def to_array(cat, dtype='d'):
    """ """
    data = []
    for key, value in cat.items():
        data.append(value)
    data = N.array(data).astype(dtype)
    return data.transpose()


if __name__=="__main__":
    with Catalogue() as C:
        C.load('spec',['num','zspec'])
        for row in C.execute("select * from spec limit 10;"):
            print row

