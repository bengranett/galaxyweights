import os
HOME = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
EXT = os.path.dirname(os.path.realpath(__file__)) + '/ext'
DATA = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + '/data'