
import catalogue
import selection

def unique_pointing(point):
    """ Rewrite 9xx pointings to 0xx """
    if point.startswith("W4P9"):
        return "W4P0"+point[4:]
    if point.startswith("W1P9"):
        return "W1P1"+point[4:]
    return point

def pointing_key(pointing, quadrant):
    """ """
    return "%sQ%s"%(pointing, quadrant)


def get_ssr_by_quadrant(sample='galaxyagn', dbfile='vipers.db', config=None):
    """ """

    with catalogue.Catalogue(dbfile=dbfile, config=config) as C:
        C.load('spec',['num','pointing','quadrant','zspec','zflg',])
        C.load('phot',['num','vmmpsflag','newflag'])

        cmd = """select zflg,pointing,quadrant,zspec
        from spec as S 
        join phot as P 
        where S.num=P.num and
        P.vmmpsflag='S' and P.newflag=1;"""

        data = C.query(cmd)
        print "Results:",len(data)

    count = {}

    for r in data:
        zflg,point,quad,zspec = r

        if selection.star(zflg, zspec): continue
        if not selection.select_targets(sample, zflg): continue

        point = unique_pointing(point)

        p = pointing_key(point, quad)

        if not count.has_key(p):
            count[p] = [0,0]

        count[p][0]+=1

        if selection.select(sample, zflg):
            count[p][1]+=1
    

    ssr = {}
    for key in count.keys():
        ssr[key] = count[key][1] * 1./count[key][0]

    return ssr


if __name__=="__main__":
    ssr_dict = get_ssr_by_quadrant()

    for key,val in ssr_dict.items():
        print key,val