

import numpy as N

MOCK = 'mock'
sample_list = ['galaxy','galaxyagn','galaxylow',MOCK]

def validate(mode):
    return mode in sample_list

def select(mode, zflag):
    """Define the target selection flags"""

    zflg = int(zflag)
    zqual = zflg%10

    if mode=='galaxy':
        if (zqual >= 2)&(zflg<10)&(zflg>0):
            return True
    elif mode=='galaxyagn':
        if (zqual >= 2)&(zflg<20)&(zflg>0):
            return True
    elif mode=='galaxylow':
        if (zflag >= 1.5)&(zflg<10)&(zflg>0):
            return True
    else:
        raise Exception("Unknown sample: %s"%mode)

def select_targets(mode, zflg):
    """Define the selection for the denominator of SSR."""
    if mode=='galaxy' or mode=='galaxylow':
        if (zflg >= 0)&(zflg < 10):
            return True
    elif mode=='galaxyagn':
        if (zflg >= 0)&(zflg < 20):
            return True
    else:
        raise Exception("Unknown sample: %s"%mode)

def get_zflg_limit(mode):
    """ """
    if mode=='galaxy' or mode=='galaxylow':
        return 10
    elif mode=='galaxyagn':
        return 20
    else:
        raise Exception("Unknown sample: %s"%mode)

def apply(mode, zflag):
    out = N.zeros(len(zflag), dtype='bool')
    for i in range(len(zflag)):
        out[i] = select(mode,zflag[i])
    return out

def star(zflag,zspec):
    """Define the star selection"""
    zflg = int(zflag)
    zqual = zflg%10

    if (zqual >= 2)&(zflg<20)&(zflg>0)&(N.abs(zspec)<1e-3):  # a star
        return True
    return False

def parent(newflag, vmmpsflag):
    """ select photometric parent"""
    select = (newflag==1)&(vmmpsflag=='S')
    return select