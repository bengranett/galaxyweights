import sys
import os
import numpy as N
import pyfits
import logging

from vipers import catalogue, selection, mask, utils
from vipers.utils import write_txt, tempname

ARCSEC_PER_DEG = 3600.

class TSR(object):
    def __init__(self, sample='galaxyagn', applyphotomask=True, aperture=None, usevenice=False, 
        parent=None, target=None, config=None):
        """ """
        self.config = config

        self.parent = parent
        self.target = target


        self.aperture = aperture
        self.usevenice = usevenice

        self.sample = sample

        self.masks = {}
        for field in config.FIELDS:
            M = mask.Mask(usevenice=usevenice, venicecmd=config.VENICECMD)
            M.add_mask_file(config.FIELDMASK%field)
            if applyphotomask:
                M.add_mask_file(config.PHOTMASK%field, holes=True)
            self.masks[field] = M

        if sample != selection.MOCK:
            self.zflag_limit = selection.get_zflg_limit(sample)

            logging.debug("building db")
            self.C = catalogue.Catalogue(config=self.config)
            self.C.load('spec', param_names=['num','zflg','alpha','delta'])
            self.C.load('phot', param_names=['num','vmmpsflag','newflag'])


    def _load_phot(self, field='W1'):
        """ """
        fits = pyfits.open(self.config.CATNAMES['phot']%field)
        ra = fits[1].data['alpha']
        dec = fits[1].data['delta']
        vmmpsflag = fits[1].data['vmmpsFlag']
        newflag = fits[1].data['newflag']

        # apply VIPERS photometric selection
        select = selection.parent(newflag, vmmpsflag)
        ra = ra[select]
        dec = dec[select]

        mask = self.masks[field]
        sel = mask.check_inside(ra, dec)

        ra = ra[sel]
        dec = dec[sel]

        return ra,dec

    def _load_spec(self, field='W1'):
        """ """

        if field=='W1':
            constraint = "alpha<100"
        else:
            constraint = "alpha>100"

        cmd = """select S.num, alpha, delta
                from spec as S 
                join phot as P 
                where S.num=P.num and 
                P.vmmpsflag='S' and P.newflag=1 
                and zflg >= 0 and zflg < %i and %s;"""%(self.zflag_limit, constraint)

        logging.debug(cmd)

        results = self.C.execute(cmd)

        logging.info("selected %s %i",field,len(results))

        num, ra, dec = utils.unzip(results)

        mask = self.masks[field]
        sel = mask.check_inside(ra, dec)

        num = num[sel]
        ra = ra[sel]
        dec = dec[sel]

        return num, ra, dec

    def _load_ascii(self, field='W1', path=None, applymask=True):
        """ """
        if not os.path.exists(path):
            raise Exception("File does not exist! %s"%path)

        num = []
        ra = []
        dec = []
        with file(path) as cat:
            for line in cat:
                line = line.strip()
                if line == "": continue
                if line.startswith("#"): continue
                id, x, y = line.split()
                num.append(int(id))
                ra.append(float(x))
                dec.append(float(y))

        num = N.array(num)
        ra = N.array(ra)
        dec = N.array(dec)

        if applymask:
            mask = self.masks[field]
            sel = mask.check_inside(ra, dec)

            num = num[sel]
            ra = ra[sel]
            dec = dec[sel]

            if len(ra)==0:
                raise Exception("No objects found within the mask in field %s."%field)

        return num, ra, dec




    def _go_DTFE(self, catalog, gridcat):
        """ Run DTFE

        cat - photometric cat
        targets - spectroscopic cat
        """

        num, x, y = gridcat

        width = N.ones(len(x))*self.aperture[0]*1./ARCSEC_PER_DEG
        height = N.ones(len(x))*self.aperture[1]*1./ARCSEC_PER_DEG

        gridfile = tempname(self.config.TMPDIR, 'grid')
        write_txt(gridfile, (x, y, width, height), count=True, boundbox=False)

        inputfile = tempname(self.config.TMPDIR, 'input')
        catxy = catalog[-2:]
        write_txt(inputfile, catxy, count=True, boundbox=True)

        output = tempname(self.config.TMPDIR, 'dtfe')
        cmd = "{dtfe} {input} {output} --padding 0 -g 1 --options {gridfile} --method 2 -s 1000 --density0 1.0 -f density_a".format(dtfe=self.config.DTFECMD, input=inputfile, output=output, gridfile=gridfile)
        logging.debug(cmd)
        os.system(cmd)

        output += ".a_den"

        dens = N.loadtxt(output)

        os.remove(gridfile)
        os.remove(inputfile)
        os.remove(output)

        return num, dens

    def compute(self, field):
        """Calculate TSR"""

        if self.sample == selection.MOCK:
            phot_cat = self._load_ascii(field=field, path=self.parent)[1:]
            spec_cat = self._load_ascii(field=field, path=self.target)
        else:
            phot_cat = self._load_phot(field)
            spec_cat = self._load_spec(field)

        info = {'nphot': len(phot_cat[0]),
                'nspec': len(spec_cat[0])}
        logging.info(info)

        num1, dens_phot = self._go_DTFE(phot_cat, spec_cat)
        num2, dens_spec = self._go_DTFE(spec_cat, spec_cat)

        assert(N.all(num1==num2))

        lookup = {}
        for i in range(len(num1)):
            if (dens_phot[i] <= 0) or (dens_spec[i] <= 0):
                tsr = -1
                logging.warn("Bad TSR: phot:%f spec:%f",dens_phot[i], dens_spec[i])
            else:
                tsr = dens_spec[i] *1./ dens_phot[i]

            if tsr > 1:
                # logging.debug("TSR > 1: phot:%f spec:%f",dens_phot[i], dens_spec[i])
                tsr = 1

            lookup[num1[i]] = tsr, dens_spec[i], dens_phot[i]

        return lookup, info
